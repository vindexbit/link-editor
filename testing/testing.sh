#!/bin/bash

BOLD='\e[1m'
LRED='\e[1;31m'
NORMAL='\e[0m'
LBLUE='\e[1;34m'
LGREEN='\e[92m'
UNDERLINED='\e[4m'
CYAN='\e[96m'

declare -i good_cases=0
declare -i bad_cases=0

CMD=""


print_fail() {
    bad_cases+=1
    echo -e "${LRED}× fail${NORMAL}"":" $CMD
}


print_pass() {
    good_cases+=1
    echo -e "  ${LBLUE}✓ pass${NORMAL}"":" $CMD
}


title() {
    echo -e "\n""${UNDERLINED}${BOLD}$@${NORMAL}"
}


procedure_title() {
    echo -e "\n>: ${BOLD}$@${NORMAL}\n"
}


case_title() {
    echo -e "  "${BOLD}${LGREEN}$@${NORMAL}
}


procedure01() {
    procedure_title "Procedure 01: Setting of new target (one link, yes-mode)"

    case_title "Link and target file exist"
    touch old_file.txt new_file.txt
    ln -s "$PWD/old_file.txt" link.txt
    CMD="./link-editor -l link.txt -p \"$PWD/new_file.txt\""
    ./link-editor -l link.txt -p "$PWD/new_file.txt"
    output=`readlink link.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output" == "$expected" ]]; then
        print_pass $CMD
    else
        echo $output
        print_fail $CMD
    fi
    rm old_file.txt new_file.txt link.txt

    case_title "Link exists, target file doesn't exist"
    touch old_file.txt
    ln -s "$PWD/old_file.txt" link.txt
    CMD="./link-editor -l link.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link.txt -p "$PWD/new_file.txt" --yes
    output=`readlink link.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output" == "$expected" && ! -e new_file.txt ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm old_file.txt link.txt

    case_title "Link doesn't exist, target file doesn't exist"
    CMD="./link-editor -l link.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link.txt -p "$PWD/new_file.txt" --yes
    output=`readlink link.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output" == "$expected" && ! -e new_file.txt ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm link.txt

    case_title "Link doesn't exist, target file exists"
    touch new_file.txt
    CMD="./link-editor -l link.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link.txt -p "$PWD/new_file.txt" --yes
    output=`readlink link.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm new_file.txt link.txt
}


procedure02() {
    procedure_title "Procedure 02: Setting of new target (one link)"

    LANG="en_US.UTF-8"

    case_title "Link exists, target file doesn't exist, answer: yes"
    touch old_file.txt
    ln -s "$PWD/old_file.txt" link.txt
    CMD="./link-editor -l link.txt -p \"$PWD/new_file.txt\""
    query=`yes | ./link-editor -l link.txt -p "$PWD/new_file.txt"`
    exp_query="$PWD/new_file.txt: this path doesn't exist.
Do you want to create a broken link? y/n >: "
    output=`readlink link.txt`
    expected="$PWD/new_file.txt"
    if [[ "$query" == "$exp_query" && "$output" == "$expected" ]]; then
        print_pass $CMD
    else
        echo $query
        echo $exp_query
        print_fail $CMD
    fi
    rm old_file.txt link.txt

    case_title "Link exists, target file doesn't exist, answer: no"
    touch old_file.txt
    ln -s "$PWD/old_file.txt" link.txt
    CMD="./link-editor -l link.txt -p \"$PWD/new_file.txt\""
    query=`yes n | ./link-editor -l link.txt -p "$PWD/new_file.txt"`
    exp_query="$PWD/new_file.txt: this path doesn't exist.
Do you want to create a broken link? y/n >: "
    output=`readlink link.txt`
    expected="$PWD/old_file.txt"
    if [[ "$query" == "$exp_query" && "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm old_file.txt link.txt

    case_title "Link doesn't exist, target file doesn't exist, answer: no"
    CMD="./link-editor -l link.txt -p \"$PWD/new_file.txt\""
    query=`yes n | ./link-editor -l link.txt -p "$PWD/new_file.txt"`
    exp_query="$PWD/new_file.txt: this path doesn't exist.
Do you want to create a broken link? y/n >: "
    if [[ "$query" == "$exp_query" && ! -e link.txt && ! -e new_file.txt ]]
    then
        print_pass $CMD
    else
        print_fail $CMD
    fi

    case_title "Link doesn't exist, target file exists, answer: yes"
    touch new_file.txt
    CMD="./link-editor -l link.txt -p \"$PWD/new_file.txt\""
    query=`yes | ./link-editor -l link.txt -p "$PWD/new_file.txt"`
    exp_query="link.txt: edited link doesn't exist.
Do you want to create it? y/n >: "
    output=`readlink link.txt`
    expected="$PWD/new_file.txt"
    if [[ "$query" == "$exp_query" && "$output" == "$expected" ]]
    then
        print_pass $CMD
    else
        print_fail $CMD
    fi
    rm new_file.txt link.txt
}


procedure03() {
    procedure_title "Procedure 03: Setting of new target (two links, yes-mode)"

    LANG="en_US.UTF-8"

    # target exists

    case_title "Link 1, link 2, target exists"
    touch old_file_1.txt old_file_2.txt new_file.txt
    ln -s "$PWD/old_file_1.txt" link_1.txt
    ln -s "$PWD/old_file_2.txt" link_2.txt
    CMD="./link-editor -l link_1.txt link_2.txt -p \"$PWD/new_file.txt\""
    ./link-editor -l link_1.txt link_2.txt -p "$PWD/new_file.txt"
    output_1=`readlink link_1.txt`
    output_2=`readlink link_2.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output_1" == "$expected" && "$output_1" == "$output_2" ]]
    then
        print_pass $CMD
    else
        echo "output_1=$output_1"
        echo "output_2=$output_2"
        echo "expected=$expected"
        print_fail $CMD
    fi
    rm old_file_1.txt old_file_2.txt new_file.txt link_1.txt link_2.txt

    case_title "Link 1 exists, link 2 doesn't exist, target exists"
    touch old_file_1.txt new_file.txt
    ln -s "$PWD/old_file_1.txt" link_1.txt
    CMD="./link-editor -l link_1.txt link_2.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link_1.txt link_2.txt -p "$PWD/new_file.txt" --yes
    output_1=`readlink link_1.txt`
    output_2=`readlink link_2.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output_1" == "$expected" && "$output_1" == "$output_2" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm old_file_1.txt new_file.txt link_1.txt link_2.txt

    case_title "Link 1 doesn't exist, link 2 doesn't exist, target exists"
    touch new_file.txt
    CMD="./link-editor -l link_1.txt link_2.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link_1.txt link_2.txt -p "$PWD/new_file.txt" --yes
    output_1=`readlink link_1.txt`
    output_2=`readlink link_2.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output_1" == "$expected" && "$output_1" == "$output_2" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm new_file.txt link_1.txt link_2.txt

    case_title "Link 1 doesn't exist, link 2 exists, target exists"
    touch old_file_2.txt new_file.txt
    ln -s "$PWD/old_file_2.txt" link_2.txt
    CMD="./link-editor -l link_1.txt link_2.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link_1.txt link_2.txt -p "$PWD/new_file.txt" --yes
    output_1=`readlink link_1.txt`
    output_2=`readlink link_2.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output_1" == "$expected" && "$output_1" == "$output_2" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm old_file_2.txt new_file.txt link_1.txt link_2.txt

    # target doesn't exist

    case_title "Link 1, link 2, target doesn't exist"
    touch old_file_1.txt old_file_2.txt
    ln -s "$PWD/old_file_1.txt" link_1.txt
    ln -s "$PWD/old_file_2.txt" link_2.txt
    CMD="./link-editor -l link_1.txt link_2.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link_1.txt link_2.txt -p "$PWD/new_file.txt" --yes
    output_1=`readlink link_1.txt`
    output_2=`readlink link_2.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output_1" == "$expected" && "$output_1" == "$output_2" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm old_file_1.txt old_file_2.txt link_1.txt link_2.txt

    case_title "Link 1 exists, link 2 doesn't exist, target doesn't exist"
    touch old_file_1.txt
    ln -s "$PWD/old_file_1.txt" link_1.txt
    CMD="./link-editor -l link_1.txt link_2.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link_1.txt link_2.txt -p "$PWD/new_file.txt" --yes
    output_1=`readlink link_1.txt`
    output_2=`readlink link_2.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output_1" == "$expected" && "$output_1" == "$output_2" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm old_file_1.txt link_1.txt link_2.txt

    case_title \
    "Link 1 doesn't exist, link 2 doesn't exist, target doesn't exist"
    CMD="./link-editor -l link_1.txt link_2.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link_1.txt link_2.txt -p "$PWD/new_file.txt" --yes
    output_1=`readlink link_1.txt`
    output_2=`readlink link_2.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output_1" == "$expected" && "$output_1" == "$output_2" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm link_1.txt link_2.txt

    case_title "Link 1 doesn't exist, link 2 exists, target doesn't exist"
    touch old_file_2.txt
    ln -s "$PWD/old_file_2.txt" link_2.txt
    CMD="./link-editor -l link_1.txt link_2.txt -p \"$PWD/new_file.txt\" --yes"
    ./link-editor -l link_1.txt link_2.txt -p "$PWD/new_file.txt" --yes
    output_1=`readlink link_1.txt`
    output_2=`readlink link_2.txt`
    expected="$PWD/new_file.txt"
    if [[ "$output_1" == "$expected" && "$output_1" == "$output_2" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm old_file_2.txt link_1.txt link_2.txt
}


procedure04() {
    procedure_title "Procedure 04: Replacing of substring in links"

    LANG="en_US.UTF-8"

    case_title "Usual substring for searhing"
    touch file.txt
    ln -s "$PWD/file.txt" link.txt
    CMD="./link-editor -l link.txt -f file -r other_object"
    $CMD
    output=`readlink link.txt`
    expected="$PWD/other_object.txt"
    if [[ "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm file.txt link.txt

    case_title "Substring for searching is full link"
    touch file.txt
    ln -s "$PWD/file.txt" link.txt
    CMD="./link-editor -l link.txt -f \"$PWD/file.txt\" -r alt_location"
    ./link-editor -l link.txt -f "$PWD/file.txt" -r alt_location
    output=`readlink link.txt`
    expected="alt_location"
    if [[ "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    rm file.txt link.txt

    case_title "Changing of nonexistent link"
    CMD="./link-editor -l link.txt -f file -r other_object"
    output=$($CMD 2>&1)
    expected="link.txt: link doesn't exist. Skipped."
    if [[ "$output" == "$expected" && ! -L link.txt  ]]
    then
        print_pass $CMD
    else
        print_fail $CMD
    fi

    case_title "Two links: existent and nonexistent"
    touch file1.txt
    ln -s "$PWD/file1.txt" link1.txt
    CMD="./link-editor -l link1.txt link2.txt -f file -r alt"
    output=$($CMD 2>&1)
    expected="link2.txt: link doesn't exist. Skipped."
    modif_link=`readlink link1.txt`
    if [[ "$output" == "$expected" && ! -L link2.txt \
       && "$modif_link" == "$PWD/alt1.txt"  ]]
    then
        print_pass $CMD
    else
        print_fail $CMD
    fi
    rm file1.txt link1.txt

    case_title "Two links: nonexistent and existent"
    touch file2.txt
    ln -s "$PWD/file2.txt" link2.txt
    CMD="./link-editor -l link1.txt link2.txt -f file -r alt"
    output=$($CMD 2>&1)
    expected="link1.txt: link doesn't exist. Skipped."
    modif_link=`readlink link2.txt`
    if [[ "$output" == "$expected" && ! -L link1.txt \
       && "$modif_link" == "$PWD/alt2.txt"  ]]
    then
        print_pass $CMD
    else
        print_fail $CMD
    fi
    rm file2.txt link2.txt

    case_title "Two links: existent and existent"
    touch file1.txt file2.txt
    ln -s "$PWD/file1.txt" link1.txt
    ln -s "$PWD/file2.txt" link2.txt
    CMD="./link-editor -l link1.txt link2.txt -f file -r alt"
    $CMD
    modif_link1=`readlink link1.txt`
    modif_link2=`readlink link2.txt`
    if [[ "$modif_link1" == "$PWD/alt1.txt" \
       && "$modif_link2" == "$PWD/alt2.txt" ]]
        then print_pass $CMD ; else print_fail $CMD
    fi
    rm file1.txt file2.txt link1.txt link2.txt
}


create_env_for_procedure05() {
    for i in {0..7} ; do touch file$i.txt ; done
    mkdir minor_dir_with_one_file
      ln -s "$PWD/file0.txt" minor_dir_with_one_file/link0.txt
    mkdir dir
      ln -s "$PWD/minor_dir_with_one_file" dir/minor_dir_lnk
      ln -s "$PWD/file1.txt" dir/link1.txt
      ln -s "$PWD/file2.txt" dir/link2.txt
      ln -s "$PWD/file3.txt" dir/link3.txt
      mkdir dir/subdir
        ln -s "$PWD/file4.txt" dir/subdir/link4.txt
        ln -s "$PWD/file5.txt" dir/subdir/link5.txt
        mkdir dir/subdir/subsubdir
          ln -s "$PWD/file6.txt" dir/subdir/subsubdir/link6.txt
          ln -s "$PWD/file7.txt" dir/subdir/subsubdir/link7.txt
}


remove_env_for_procedure05() {
    rm -rf file*.txt dir minor_dir_with_one_file
}


links_of_dir_changed() {
    real_res1=`readlink dir/link1.txt`
    real_res2=`readlink dir/link2.txt`
    real_res3=`readlink dir/link3.txt`
    real_res4=`readlink dir/minor_dir_lnk`
    exp_res1="$PWD/other_object1.txt"
    exp_res2="$PWD/other_object2.txt"
    exp_res3="$PWD/other_object3.txt"
    exp_res4="$PWD/minor_dir_with_one_other_object"
    [[ "$real_res1" == "$exp_res1"
    && "$real_res2" == "$exp_res2" 
    && "$real_res3" == "$exp_res3" 
    && "$real_res4" == "$exp_res4" ]]
}


links_of_subdir_changed() {
   [[ `readlink dir/subdir/link4.txt` == "$PWD/other_object4.txt"
   && `readlink dir/subdir/link5.txt` == "$PWD/other_object5.txt" ]]
}


links_of_subsubdir_changed() {
   [[ `readlink dir/subdir/subsubdir/link6.txt` == "$PWD/other_object6.txt"
   && `readlink dir/subdir/subsubdir/link7.txt` == "$PWD/other_object7.txt" ]]
}

links_of_minor_dir_changed() {
    LINK0=minor_dir_with_one_file/link0.txt
    [[ `readlink $LINK0` == "$PWD/other_object0.txt" ]]
}

update_results() {
    #values of results: 0 - changes exist; 1 - changes no exist
    links_of_dir_changed;       res1=$?
    links_of_subdir_changed;    res2=$?
    links_of_subsubdir_changed; res3=$?
    links_of_minor_dir_changed; res4=$?
}

procedure05() {
    procedure_title \
    "Procedure 05: Substring replacement in links for specified directory"

    LANG="en_US.UTF-8"

    case_title "Usual substring for searhing"
    create_env_for_procedure05
    CMD="./link-editor -d dir -f file -r other_object" ; $CMD
    update_results
    if [[ $res1 -eq 0 && $res2 -eq 1 && $res3 -eq 1 && $res4 -eq 1 ]]
    then
        print_pass $CMD
    else
        [[ $res1 -eq 0 ]] && echo 1 || echo F
        [[ $res2 -eq 1 ]] && echo 2 || echo F
        [[ $res3 -eq 1 ]] && echo 3 || echo F
        [[ $res4 -eq 1 ]] && echo 4 || echo F
        print_fail $CMD
    fi
    remove_env_for_procedure05

    case_title "Recursive replacing"
    create_env_for_procedure05
    CMD="./link-editor -d dir -f file -r other_object -R" ; $CMD
    update_results
    if [[ $res1 -eq 0 && $res2 -eq 0 && $res3 -eq 0 && $res4 -eq 1 ]]
        then print_pass $CMD ; else print_fail $CMD
    fi
    remove_env_for_procedure05
    
    case_title "Non-existent directory"
    CMD="./link-editor -d dir2 -f file -r other_object -R" ;
    output=`$CMD 2>&1`
    expected="dir2: directory doesn't exist. Skipped."
    if [[ "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi

    case_title "Directory is link"
    create_env_for_procedure05
    CMD="./link-editor -d dir/minor_dir_lnk -f file -r other_object" ; $CMD
    update_results
    if [[ $res1 -eq 1 && $res2 -eq 1 && $res3 -eq 1 && $res4 -eq 0 ]]
        then print_pass $CMD ; else print_fail $CMD
    fi
    remove_env_for_procedure05

    case_title "Two directories: non-existent and existent"
    create_env_for_procedure05
    CMD="./link-editor -d dir2 dir -f file -r other_object"
    output=`$CMD 2>&1`
    expected="dir2: directory doesn't exist. Skipped."
    update_results
    if [[ $res1 -eq 0 && $res2 -eq 1 && $res3 -eq 1 && $res4 -eq 1
     && "$output" == "$expected" ]]
        then print_pass $CMD ; else print_fail $CMD
    fi
    remove_env_for_procedure05

    case_title "Two directories: existent and non-existent"
    create_env_for_procedure05
    CMD="./link-editor -d dir dir2 -f file -r other_object"
    output=`$CMD 2>&1`
    expected="dir2: directory doesn't exist. Skipped."
    update_results
    if [[ $res1 -eq 0 && $res2 -eq 1 && $res3 -eq 1 && $res4 -eq 1
     && "$output" == "$expected" ]]
        then print_pass $CMD ; else print_fail $CMD
    fi
    remove_env_for_procedure05
 
    case_title "Two directories: existent and existent"
    create_env_for_procedure05
    CMD="./link-editor -d dir minor_dir_with_one_file -f file -r other_object"
    $CMD
    update_results
    if [[ $res1 -eq 0 && $res2 -eq 1 && $res3 -eq 1 && $res4 -eq 0 ]]
        then print_pass $CMD ; else print_fail $CMD
    fi
    remove_env_for_procedure05
}


procedure06() {
    procedure_title \
    "Procedure 06: Incorrect options"

    SYSLANG=$LANG
    LANG=en_US.UTF-8

    CMD="./link-editor -l link*.txt -x overwrite -w off"
    output=`$CMD 2>&1`
    expected="Wrong using of link-editor.
See: link-editor --help"
    if [[ "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi
    
    CMD="./link-editor -l link1.txt -p fff -x overwrite -w off"
    output=`$CMD 2>&1`
    expected="Wrong using of link-editor.
See: link-editor --help"
    if [[ "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi

    CMD="./link-editor --yes -l link1.txt -r 1 -f 2 -x overwrite -w off"
    output=`$CMD 2>&1`
    expected="Wrong using of link-editor.
See: link-editor --help"
    if [[ "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi

    LANG=$SYSLANG
}


procedure07() {
    procedure_title \
    "Procedure 07: Additional options"

    CMD="./link-editor --version"
    output=`$CMD 2>&1`
    expected="`cat ../source/version`"
    if [[ "$output" == "$expected" ]]
        then print_pass $CMD ; else echo $output ; print_fail $CMD
    fi

}


run_tests() {
    # title "First mode of link-editor: setting of new path for links"
    procedure01
    procedure02
    procedure03

    # title "Second mode of link-editor"
    procedure04

    # title "Third mode of link-editor"
    procedure05

    # title "Additional tests"
    procedure06
    procedure07
}
run_tests


echo -e "$BOLD$CYAN"
echo "Good cases: $good_cases"
echo "Bad cases:  $bad_cases"
echo -e "$NORMAL"

if [[ $bad_cases -ne 0 ]]; then
    exit 1
fi
