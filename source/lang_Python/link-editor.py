#!/usr/bin/env python3

# Copyright (C) 2019-2020 Eugene 'Vindex' Stulin
#
# link-editor is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import sys, os
import getopt

locale_strings = list()
lang_conformity = dict()
current_language = "en_US"

def get_system_language():
    locale = os.environ.get("LANG")
    if locale is not None:
        return locale.split(".")[0]
    return "en_US"


def get_current_language():
    return(current_language)


def init_localization(strings_with_localization, language = ""):
    global locale_strings
    locale_strings = strings_with_localization
    if locale_strings[0][0] != "en_US":
        raise LocalizationTableException(\
            "Error: array in cell[0][0] must contain 'en_US'")
    if language == "":
        language = get_system_language()
    choose_language(language)


def choose_language(language):
    global locale_strings
    global lang_conformity
    global current_language
    if not locale_strings:
        return()
    for i, locale in enumerate(locale_strings[0]):
        if language != locale:
            continue
        current_language = language
        current_table_column = i
        for row in locale_strings:
            if row[current_table_column] == "":
                lang_conformity[row[0]] = row[0]
            else:
                lang_conformity[row[0]] = row[current_table_column]
        return()
    sys.stderr.write("The localization " + language + " is not valid.\n" +
        "The English language (en_US) is used.\n")
    current_language = "en_US"


def s_(english_string):
    global lang_conformity
    return(lang_conformity.get(english_string, english_string))


class LocalizationsTableException(Exception):
    pass


#
# main part of the program
#


APP = "link-editor"
APP_VERSION = "0.7.7"

translations =\
[

[
    "en_US", "ru_RU", "eo"
],
[
    "Wrong using of link-editor.",
    "Неправильное использование link-editor.",
    "Neregula uzo de link-editor."
],
[
    ": this path doesn't exist.",
    ": этот путь не существует.",
    ": ĉi tiu vojo ne ekzistas."
],
[
    "Do you want to create it? y/n >: ",
    "Вы хотите её создать? д/н >: ",
    "Ĉu vi volas ekzistigi ĝin? j/n >: "
],
[
    "Do you want to create a broken link? y/n >: ",
    "Вы хотите создать битую ссылку? д/н >: ",
    "Ĉu vi volas krei rompitan ligilon? j/n >: "
],
[
    ": edited link doesn't exist.",
    ": редактируемая ссылка не существует.",
    ": redaktata ligilo ne ekzistas."
],
[
    ": link doesn't exist.",
    ": ссылка не существует.",
    ": ligilo ne ekzistas."
],
[
    ": directory doesn't exist.",
    ": директория не существует.",
    ": dosierujo ne ekzistas."
],
[
    ": it is not symbolic link.",
    ": это не символическая ссылка",
    ": ĝi ne estas simbola ligilo"
],
[
    ": it is not directory.",
    ": это не директория.",
    ": ĝi ne estas dosierujo."
],
[
    "Skipped.",
    "Пропускаем.",
    "Preterlasas."
],
[
    "See: link-editor --help",
    "Смотрите: link-editor --help",
    "Rigardu: link-editor --help"
],
[
    "yes",
    "да",
    "jes"
],
[
    "y",
    "д",
    "j" ],
[
    "no", 
    "нет",
    "ne"
],
[
    "n",
    "н",
    "n"
]

]

# end of translations


ABS_PATH = os.path.abspath(os.path.dirname(sys.argv[0]))
SHARE_DIR = os.path.abspath(ABS_PATH + "../share")

app_mode = ""

def show_help():
    help_dir_path = SHARE_PATH + "/help/" + current_language + "/" + APP
    help_file_path = help_dir_path  + "/help.txt"
    with open(help_file_path, 'r') as help_file:
        print(help_file.read())


def show_version():
    print(APP_VERSION)


def get_positive_answer():
    answer = input().strip().lower() 
    return(answer in [s_("y"), s_("yes"), "y", "yes", ""])


def set_links(links, path_for_setting, yes_mode):
    if not os.path.exists(path_for_setting) and not yes_mode:
        print(path_for_setting + s_(": this path doesn't exist."))
        print(s_("Do you want to create a broken link? y/n >: "), end='')
        if not get_positive_answer():
            return()
    for link in links:
        if not os.path.exists(link) and not yes_mode:
            print(link + s_(": edited link doesn't exist."))
            print(s_("Do you want to create it? y/n >: "))
            if not get_positive_answer():
                continue
        if os.path.exists(link):
            os.remove(link)
        os.symlink(path_for_setting, link)


def check_file(filepath):
    global app_mode
    assert app_mode in ["simple_replacing", "replacing_in_directory"]
    if app_mode == "simple_replacing":
        if not os.path.exists(filepath):
            sys.stderr.write(filepath + s_(": link doesn't exist."))
            return False
        elif not os.path.islink(filepath):
            sys.stderr.write(filepath + s_(": it is not symbolic link."))
            return False
    elif app_mode == "replacing_in_directory":
        if not os.path.exists(filepath):
            sys.stderr.write(filepath + s_(": directory doesn't exist."))
            return False
        elif not os.path.isdir(filepath):
            sys.stderr.write(filepath, s_(": it is not directory."))
            return False
    return True


def extract_range_for_option(args, option, alt_option = ""):
    res_range = list()
    index1 = -1
    index2 = -1
    if option in args:
        index1 = args.index(option)
    if index1 == -1 and alt_option in args:
        index1 = args.index(alt_option)
    if index1 == -1:
        return(args, res_range)
    if index1 != len(args)-1 and not args[index1 + 1].startswith("-"):
        for i, arg in enumerate(args[index1+1:]):
            if arg.startswith("-"):
                index2 = index1 + i
                break
        if index2 != -1:
            res_range = args[index1+1 : index2+1]
            args = args[0 : index1] + args[index2+1:]
        else:
            index2 = len(args) - 1
            res_range = args[index1+1 : index2+1]
            args = args[0 : index1]
    return(args, res_range)


def extract_value_for_option(args, option, alt_option = ""):
    value = ""
    index1 = -1
    for i, arg in enumerate(args):
        if arg == option or arg.startswith(option+'='):
            index1 = i
            break
        elif arg == alt_option or arg.startswith(alt_option+'='):
            index1 = i
            break
    if index1 == -1:
        return(args, "")
    if args[index1].startswith(option+'='):
        value = args[index1][len(option+'='):]
        temp_args = args[0:index1]
        if len(args) > index1+1:
            temp_args += args[index1+1:]
        args = temp_args
    else:
        if len(args) < index1+2:
            return(args, "")
        value = args[index1+1]
        temp_args = args[0:index1]
        if len(args) > index1+2:
            temp_args += args[index1+2:]
        args = temp_args
    return(args, value)


def replace_parts_of_links(
        search_paths,
        substring_for_finding,
        replacement_string,
        recursively = False):
    for p in search_paths:
        link_list = list()
        if not check_file(p):
            sys.stderr.write(" " + s_("Skipped.") + "\n")
            continue
        if app_mode == "replacing_in_directory" and os.path.islink(p):
            p = os.readlink(p)

        def fill_link_list(p):
            nonlocal recursively
            nonlocal link_list
            if os.path.islink(p):
                link_list.append(p)
            elif os.path.isdir(p) and recursively:
                file_list = os.listdir(p)
                for i in range(0,len(file_list)):
                    file_list[i] = p + "/" + file_list[i]
                for el in file_list:
                    fill_link_list(el)
            elif os.path.isdir(p):
                file_list = os.listdir(p)
                for i in range(0,len(file_list)):
                    file_list[i] = p + "/" + file_list[i]
                file_list = list(filter(lambda x: os.path.islink(x), file_list))
                if file_list is not None:
                    link_list += file_list

        fill_link_list(p)
        for link in link_list:
            cur_address = os.readlink(link)
            new_address = cur_address.replace(
                substring_for_finding,
                replacement_string)
            if cur_address != new_address:
                os.remove(link)
                os.symlink(new_address, link)


def main():
    global app_mode
    args = sys.argv
    init_localization(translations)
    wrong_using_msg = s_("Wrong using of link-editor.")
    
    if len(args) == 1:
        sys.stderr.write(wrong_using_msg + "\n")
        sys.stderr.write(s_("See: link-editor --help") + "\n")
        return

    links = list()
    dirs = list()
    path_for_setting = ""
    yes_mode = False
    substring_for_finding = ""
    replacement_string = ""
    recursively = False

    if args[1] == "--help":
        app_mode = "help_display"
        args.remove("--help")
    elif args[1] == "--version":
        app_mode = "version_display"
        args.remove("--version")
    else:
        args, links = extract_range_for_option(args, "-l")
        args, path_for_setting = extract_value_for_option(args, "-p")
        if path_for_setting != "": # setting new link path
            app_mode = "setting"
            if "-y" in args:
                yes_mode = True
                args.remove("-y")
            if "--yes" in args:
                yes_mode = True
                args.remove("--yes")
        else:
            args, substring_for_finding = extract_value_for_option(args, "-f")
            args, replacement_string = extract_value_for_option(args, "-r")
            if links != []:
                app_mode = "simple_replacing"
            else:
                app_mode = "replacing_in_directory"
                args, dirs = extract_range_for_option(args, "-d")
                if "-R" in args:
                    recursively = True
                    args.remove("-R")

        if len(args) != 1:
            sys.stderr.write(wrong_using_msg + "\n")
            sys.stderr.write(s_("See: link-editor --help") + "\n")
            return()

    if app_mode == "help_display":
        show_help()
    elif app_mode == "version_display":
        show_version()
    elif app_mode == "setting":
        set_links(links, path_for_setting, yes_mode)
    elif app_mode == "simple_replacing":
        replace_parts_of_links(
            links,
            substring_for_finding,
            replacement_string
        )
    elif app_mode == "replacing_in_directory":
        replace_parts_of_links(
            dirs,
            substring_for_finding,
            replacement_string,
            recursively
        )

main()
