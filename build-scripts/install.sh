#!/bin/bash -e
source build-scripts/env.sh

MODE="$1"
PREFIX="$2"

if [[ ${PREFIX} != "" ]]; then
    BASE="${PREFIX}"
else
    BASE=${DEFAULT_INST_ROOTDIR}
fi

read -ra distr_id_arr <<< "`lsb_release -i`"
# lsb_release -i (for example): "Distributor ID: openSUSE"
DISTRIBUTION=${distr_id_arr[2]} # for example, "openSUSE"

readonly DEFAULT_ROOTDIR=/usr
readonly DEFAULT_ALTROOTDIR=/usr/local
# RL is "Relative Location"
readonly RL_BINDIR=bin
readonly RL_SHAREDIR=share
readonly RL_HELPDIR=${RL_SHAREDIR}/help
readonly RL_HELPDIR_EN=${RL_HELPDIR}/en_US/${APP}
readonly RL_HELPDIR_RU=${RL_HELPDIR}/ru_RU/${APP}
readonly RL_HELPDIR_EO=${RL_HELPDIR}/eo/${APP}

if [[ "${DISTRIBUTION}" != "openSUSE" ]]; then
    readonly RL_DOCDIR=${RL_SHAREDIR}/doc
else
    readonly RL_DOCDIR=${RL_SHAREDIR}/doc/packages
fi

readonly RL_DOCDIR_SPEC=${RL_DOCDIR}/${APP}
readonly RL_MANDIR=${RL_SHAREDIR}/man
readonly RL_MANDIR_EN=${RL_MANDIR}/man1
readonly RL_MANDIR_RU=${RL_MANDIR}/ru/man1
readonly RL_MANDIR_EO=${RL_MANDIR}/eo/man1
readonly RL_BASHCOMP=${RL_SHAREDIR}/bash-completion/completions

INST_BINDIR="${BASE}/${RL_BINDIR}"
INST_MANDIR_EN="${BASE}/${RL_MANDIR_EN}"
INST_MANDIR_RU="${BASE}/${RL_MANDIR_RU}"
INST_MANDIR_EO="${BASE}/${RL_MANDIR_EO}"
INST_BASHCOMPDIR="${BASE}/${RL_BASHCOMP}"
INST_HELPDIR="${BASE}/${RL_HELPDIR}"

INST_DOCDIR_SPEC="${BASE}/${RL_DOCDIR_SPEC}"
INST_HELPDIR_EN="${BASE}/${RL_HELPDIR_EN}"
INST_HELPDIR_RU="${BASE}/${RL_HELPDIR_RU}"
INST_HELPDIR_EO="${BASE}/${RL_HELPDIR_EO}"

set -x

echo "PREFIX:" ${BASE}

if [[ ${MODE} == "--install" ]]; then
    # prepare directories
    mkdir -p "${INST_BINDIR}" "${INST_BASHCOMPDIR}" "${INST_DOCDIR_SPEC}"

    # install executable file and bash completion script
    install "${BINPATH}" "${INST_BINDIR}/"
    if [[ -e source/_${APP} ]]; then
        cp source/_${APP} "${INST_BASHCOMPDIR}/${APP}"
    fi

    # copy copyright file
    cp copyright ${INST_DOCDIR_SPEC}/

    # install help files
    mkdir -p ${INST_HELPDIR_EN} ${INST_HELPDIR_RU} ${INST_HELPDIR_EO}
    cp help/help_en_US.txt ${INST_HELPDIR_EN}/help.txt
    cp help/help_ru_RU.txt ${INST_HELPDIR_RU}/help.txt
    cp help/help_eo.txt    ${INST_HELPDIR_EO}/help.txt

    # install man-pages
    mkdir -p ${INST_MANDIR_EN} ${INST_MANDIR_RU} ${INST_MANDIR_EO}
    gzip -9 -k -n help/${APP}.1
    gzip -9 -k -n help/${APP}.ru.1
    gzip -9 -k -n help/${APP}.eo.1
    install help/${APP}.1.gz    ${INST_MANDIR_EN}/${APP}.1.gz
    install help/${APP}.ru.1.gz ${INST_MANDIR_RU}/${APP}.1.gz
    install help/${APP}.eo.1.gz ${INST_MANDIR_EO}/${APP}.1.gz
    rm help/*.1.gz
else
    rm -f ${INST_BINDIR}/${APP}
    rm -f ${INST_BASHCOMPDIR}/${APP}
    rm -rf ${INST_DOCDIR_SPEC}
    rm -rf ${INST_HELPDIR_EN}
    rm -rf ${INST_HELPDIR_RU}
    rm -rf ${INST_HELPDIR_EO}
    rm -f ${INST_MANDIR_EN}/${APP}.1.gz
    rm -f ${INST_MANDIR_RU}/${APP}.1.gz
    rm -f ${INST_MANDIR_EO}/${APP}.1.gz
fi


set +x
