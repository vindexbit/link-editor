#!/bin/bash -e
source build-scripts/env.sh

SRCDIR=source/lang_D
SRC="${SRCDIR}/main.d ${SRCDIR}/additional_functions.d"

BINVERSION=$1      # release or debug
DC=$2              # dmd, ldc2 or gdc
PHOBOS_LINKING=$3  # dynamic or static

LDC2_STATIC_OPTS="-defaultlib=:libphobos2-ldc.a,:libdruntime-ldc.a,:libz.a"
LDC2_STATIC_OPTS+=" -link-defaultlib-shared=false"
GDC_STATIC_OPTS="-static-libphobos"
DMD_STATIC_OPTS=""

if [[ ${PHOBOS_LINKING} == static ]]; then
    STATIC_OPTS=$(eval echo \$${DC^^}_STATIC_OPTS)
fi

if [[ ${DC} == dmd ]]; then
    DEBUG="-debug -g"
    RELEASE="-O -release"
    OUT="-of"
elif [[ ${DC} == ldc2 ]]; then
    DEBUG="-d-debug -gc"
    RELEASE="-O -release"
    OUT="-of"
elif [[ ${DC} == gdc ]]; then
    DEBUG="-fdebug"
    RELEASE="-O2 -frelease"
    OUT="-o "
fi

set -x

mkdir -p ${BINPATH_BASEDIR}
if [[ $BINVERSION == debug ]]; then
    ${DC} ${SRC} ${OUT}${BINPATH} -Jsource/ ${STATIC_OPTS} ${DEBUG}
else
    ${DC} ${SRC} ${OUT}${BINPATH} -Jsource/ ${STATIC_OPTS} ${RELEASE}
    objcopy --strip-debug --strip-unneeded ${BINPATH} ${BINPATH}
fi
pushd "${BINPATH_BASEDIR}"
chrpath -d ${APP}
chmod 755 ${APP}
popd

set +x
