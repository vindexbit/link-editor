#!/bin/bash -e
source build-scripts/env.sh
SRCDIR=source/lang_Python
SRC="${SRCDIR}/link-editor.py"
MAIN_SRC_FILE="${SRCDIR}/link-editor.py"

mkdir -p ${BINPATH_BASEDIR}/
set -x

declare -i index1
declare -i index2
grep_answer=`grep -n "translations =" ${SRCDIR}/link-editor.py`
index1=`echo ${grep_answer} | sed "s/:translations =\\\\\\//g"`
grep_answer=`grep -n "# end of translations" ${SRCDIR}/link-editor.py`
index2=`echo ${grep_answer} | sed "s/:\# end of translations//g"`
let "index1 += 1"
let "index2 -= 1"
sed "${index1},${index2}d" ${SRCDIR}/link-editor.py > temp.py
let "index1 -= 1"
sed  "${index1}r source/translations.dtxt" temp.py > ${SRCDIR}/link-editor.py
rm temp.py

sed -i "s/^APP_VERSION.*/APP_VERSION = \"${VERSION}\"/g" "${MAIN_SRC_FILE}"

cp ${SRCDIR}/link-editor.py ${BINPATH}
chmod 755 ${BINPATH}

set +x
